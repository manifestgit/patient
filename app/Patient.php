<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    protected $fillable = ['ref_code','name','mobile'];
    
    public function userData(){

    	$this->belongsTo('App\User');
    	
    }
}
