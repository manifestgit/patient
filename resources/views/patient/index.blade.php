@extends('layouts.app')

@section('content')
 

<div class="container">
    <div class="row">
        <div class="col-md-12 ">

                    
         
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title float-left">Patient Members</h3>
              <a href="{{route('patient.create')}}" class="btn btn-success float-right"> Add New </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive table">
              <table id="patient_list_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Code </th>
                  <th>Name</th>
                  <th>Mobile</th>
                  
                 
                </tr>
                </thead>
                <tbody>
                  @foreach($patients as $patient)

                    <tr>
                      <td>{{$patient->ref_code}}</td>
                      <td>{{$patient->name}}</td>
                      <td>{{$patient->mobile}}</td>
                    </tr>
                  @endforeach
                  
                </tbody>
                
              </table>
              </div>
            </div>
            <!-- /.box-body -->
         
        </div>
    </div>
</div>
</div>


@endsection
